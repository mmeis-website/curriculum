FROM node:alpine as builder

WORKDIR /app

COPY package*.json ./

RUN npm i && npm install -g vue

COPY . .

RUN npm run build


FROM nginx:1.19-alpine

COPY --from=builder /app/dist/ /var/www/
