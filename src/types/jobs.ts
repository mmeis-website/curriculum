export enum Jobs {
  Fullstack = 'Fullstack',
  Backend = 'Backend',
  Frontend = 'Frontend',
  Software = 'Software'
}

// eslint-disable-next-line
export function isJob (job: any): job is Jobs {
  return typeof job === 'string' && Object.keys(Jobs).indexOf(job) >= 0
}
